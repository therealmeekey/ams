from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from ams import settings
from django.contrib.auth import views as auth_views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib.auth.decorators import login_required
from ams import views

urlpatterns = [
    path('', login_required(views.index)),
    path('admin/', admin.site.urls),

    path('helpdesk/', include('helpdesk.urls')),
    path('account/', include('account.urls')),

    path('account/login/', auth_views.LoginView.as_view(),  name='login'),
    path('logout/', auth_views.LogoutView.as_view(), {'next_page': '/account/login/'}, name='logout')
]

urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_URL)
