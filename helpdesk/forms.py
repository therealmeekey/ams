from django import forms
from helpdesk.models import Application

# Форма для добавления/изменения заявки


class AppForm(forms.ModelForm):

	class Meta:
		model = Application
		fields = ('title', 'text', 'room', 'phone')
		
	def __init__(self, *args, **kwargs):
		super(AppForm, self).__init__(*args, **kwargs)
		self.fields['text'].widget.attrs.update({'class': 'materialize-textarea'})
