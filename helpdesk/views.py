from django.shortcuts import render, get_object_or_404, redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from helpdesk.models import Application, Executor
from django.utils import timezone
from helpdesk.forms import AppForm


# Pages:

# Все заявки

def application_list(request):
    if request.user.has_perm('helpdesk.can_close'):
        object_list = Application.objects.filter(status__in=['New', 'In the work'])
        paginator = Paginator(object_list, 8)
        page = request.GET.get('page')
        try:
            posts = paginator.page(page)
        except PageNotAnInteger:
            posts = paginator.page(1)
        except EmptyPage:
            posts = paginator.page(paginator.num_pages)
        return render(request, 'helpdesk/application_list.html', {'posts': posts, 'page': page, })
    else:
        return render(request, 'helpdesk/error.html')


# Мои заяки
def my_application(request):
    if request.user.has_perm('helpdesk.can_close') or request.user.has_perm('helpdesk.can_assign'):
        owners = Executor.objects.filter(owner__username=request.user)
        owner_posts = Application.objects.filter(status='In the work', executor__in=owners)
        paginator = Paginator(owner_posts, 4)
        page = request.GET.get('page')
        try:
            owner = paginator.page(page)
        except PageNotAnInteger:
            owner = paginator.page(1)
        except EmptyPage:
            owner = paginator.page(paginator.num_pages)
        auth_posts = Application.objects.filter(author__username=request.user,
                                                status__in=['New', 'In the work'])
        paginator = Paginator(auth_posts, 4)
        page = request.GET.get('page')
        try:
            auths = paginator.page(page)
        except PageNotAnInteger:
            auths = paginator.page(1)
        except EmptyPage:
            auths = paginator.page(paginator.num_pages)
        return render(request, 'helpdesk/my_application.html', {'owner': owner, 'auths': auths})
    else:
        return render(request, 'helpdesk/error.html')


# История
def history(request):
    if request.user.has_perm('helpdesk.can_close') or request.user.has_perm('helpdesk.can_assign'):
        owners = Executor.objects.filter(owner__username=request.user)
        app_history_owners = Application.objects.filter(status='Complited',
                                                        executor__in=owners)
        paginator = Paginator(app_history_owners, 10)
        page = request.GET.get('page')
        try:
            owner_history = paginator.page(page)
        except PageNotAnInteger:
            owner_history = paginator.page(1)
        except EmptyPage:
            owner_history = paginator.page(paginator.num_pages)
        app_history_auths = Application.objects.filter(status='Complited',
                                                       author__username=request.user)
        paginator = Paginator(app_history_auths, 10)
        page = request.GET.get('page')
        try:
            auth_history = paginator.page(page)
        except PageNotAnInteger:
            auth_history = paginator.page(1)
        except EmptyPage:
            auth_history = paginator.page(paginator.num_pages)
        return render(request, 'helpdesk/application_history.html',
                      {'owner_history': owner_history, 'auth_history': auth_history})
    else:
        return render(request, 'helpdesk/error.html')


# Детальный просмотр заявки
def application_detail(request, pk):
    if request.user.has_perm('helpdesk.can_close') or request.user.has_perm('helpdesk.can_assign'):
        post = get_object_or_404(Application, pk=pk)
        executor = post.executor_set.filter(owner__username=request.user)
        return render(request, 'helpdesk/application_detail.html',
                      {'post': post, 'executor': executor})
    else:
        return render(request, 'helpdesk/error.html')


# Button:

# Закрыть заявку
def complited(request, pk):
    post = get_object_or_404(Application, pk=pk)
    if post.executor_set.filter(owner__username=request.user):
        post = get_object_or_404(Application, pk=pk)
        post.status = 'Complited'
        post.save()
        return redirect('application_detail', pk=post.pk)
    else:
        return redirect('application_detail', pk=post.pk)


# Выполнить заявку
def perform(request, pk):
    post = get_object_or_404(Application, pk=pk)
    if request.user.has_perm('helpdesk.can_close'):
        if post.executor_set.filter(owner=request.user):
            post.status = 'In the work'
        else:
            post.status = 'In the work'
            post.executor_set.create(owner=request.user)
            post.save()
        return redirect('application_detail', pk=post.pk)
    else:
        return redirect('application_detail', pk=post.pk)


# Снять заявку
def take_off(request, pk):
    post = get_object_or_404(Application, pk=pk)
    if request.user.has_perm('helpdesk.can_close'):
        post.executor_set.filter(owner=request.user).delete()
        if post.executor_set.count() == 0:
            post.status = 'New'
        else:
            post.status = 'In the work'
        post.save()
        return redirect('application_detail', pk=post.pk)
    else:
        return redirect('application_detail', pk=post.pk)


# Изменение заявки
def application_edit(request, pk):
    post = get_object_or_404(Application, pk=pk)
    if request.user == post.author:
        if request.method == "POST":
            form = AppForm(request.POST, instance=post)
            if form.is_valid():
                post = form.save(commit=False)
                post.author = request.user
                post.published_date = timezone.now()
                post.save()
                return redirect('application_detail', pk=post.pk)
        else:
            form = AppForm(instance=post)
        return render(request, 'helpdesk/application_edit.html', {'form': form})
    else:
        return redirect('application_detail', pk=post.pk)


# Создание новой заявки
def application_new(request):
    if request.user.has_perm('helpdesk.can_close') or request.user.has_perm('helpdesk.can_assign'):
        if request.method == "POST":
            form = AppForm(request.POST)
            if form.is_valid():
                post = form.save(commit=False)
                post.author = request.user
                post.published_date = timezone.now()
                post.status = 'New'
                post.save()
                return redirect('application_detail', pk=post.pk)
        else:
            form = AppForm()
        return render(request, 'helpdesk/application_edit.html', {'form': form})
    else:
        return render(request, 'helpdesk/error.html')
