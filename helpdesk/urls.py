from django.urls import path
from django.contrib.auth.decorators import login_required
from helpdesk import views


urlpatterns = [
	# Мои заявки
	path('my_application/', login_required(views.my_application), name='my_application'),
	# Все заявки
	path('application_list/', login_required(views.application_list), name='application_list'),
	# Детальный просмотр заявки
	path('application/<int:pk>/', login_required(views.application_detail), name='application_detail'),
	# Создание новой заявки
	path('application_new/', login_required(views.application_new), name='application_new'),
	# Изменение заявки
	path('application/<int:pk>/edit/', login_required(views.application_edit), name='application_edit'),
	# История
	path('application_history/', login_required(views.history), name='application_history'),
	# Закрыть заявку
	path('application/<int:pk>/complited', login_required(views.complited), name='complited'),
	# Выполнить заявки
	path('application/<int:pk>/perform', login_required(views.perform), name='perform'),
	# Снять заявку
	path('application/<int:pk>/takeoff', login_required(views.take_off), name='take_off'),
]
