from django.urls import path
from django.contrib.auth.decorators import login_required
from account import views

urlpatterns = [
    # Изменение профиля
    path('profile_edit/<str:username>', login_required(views.edit_profile), name='edit_profile'),
    # Просмотр профиля
    path('profile/<str:username>', login_required(views.view_profile), name='profile'),
]